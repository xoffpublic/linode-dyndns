#!/bin/bash

# Script to set IP via Linode API v4

# Add your API key here.
# Get an API key here: https://cloud.linode.com/profile/tokens
# DO NOT CHECK THIS INTO GIT!
TOKEN=

# Domain record number - get from running: curl -H "Authorization: Bearer $TOKEN" https://api.linode.com/v4/domains
DOMAIN=

# Record number for the A record - get from running this and looking for your record: curl -H "Authorization: Bearer $TOKEN"  https://api.linode.com/v4/domains/RECORD_FROM_PREVIOUS_COMMAND
RECORD=

# Note on TTL: It can't be less than 300 (seconds) and has to be one of these values:
# 300, 3600, 7200, 14400, 28800, 57600, 86400, 172800, 345600, 604800, 1209600, and 2419200 - any other value will be rounded to the nearest valid value.

# Get the IP address from anywhere that will echo it
IP=`curl -s http://ipecho.net/plain`

curl -H "Content-Type: application/json" \
    -H "Authorization: Bearer $TOKEN" \
    -X PUT -d '{
      "type": "A",
      "target": "'"$IP"'",
      "ttl_sec": 300
    }' \
    https://api.linode.com/v4/domains/$DOMAIN/records/$RECORD

